package com.example.studicase;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private EditText Panjang;
    private EditText Lebar;
    private EditText Luas;
    private Button check;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initUI();
        initEvent();
    }
    private void initUI() {
        Panjang = (EditText) findViewById(R.id.panjangg);
        Lebar = (EditText) findViewById(R.id.lebarr);
        Luas = (EditText) findViewById(R.id.luass);
        check = (Button) findViewById(R.id.cek);
    }

    private void initEvent(){
        check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hitungluas();
            }
        });
    }
    private void hitungluas() {
        int panjangg = Integer.parseInt(Panjang.getText().toString());
        int lebarr = Integer.parseInt(Lebar.getText().toString());
        int luass = panjangg*lebarr;
        Luas.setText(luass+"");
    }

}
